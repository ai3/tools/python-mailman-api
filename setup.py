#!/usr/bin/python

from setuptools import setup, find_packages


setup(
    name="mailman-api",
    version="0.1",
    description="Internal Mailman API server",
    author="Autistici/Inventati",
    author_email="info@autistici.org",
    url="https://git.autistici.org/ai3/python-mailman-api",
    install_requires=["Flask"],
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "python-mailman-api-server = mailman_api.mailman:main",
        ],
    },
)

