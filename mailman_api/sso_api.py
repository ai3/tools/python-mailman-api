import os
import sso
from functools import wraps
from flask import current_app, request, make_response, abort, g


def init_sso(app):
    if 'SSO_SERVICE' not in app.config:
        raise Exception('Must configure SSO_SERVICE')
    if 'SSO_DOMAIN' not in app.config:
        raise Exception('Must configure SSO_DOMAIN')

    pubkey_file = app.config.get(
        'SSO_PUBLIC_KEY_FILE', '/etc/sso/public.key')
    with open(pubkey_file) as f:
        pubkey = f.read()

    # Ensure the login server URL is /-terminated.
    app.sso_service = app.config['SSO_SERVICE']
    app.sso_validator = sso.Verifier(
        pubkey, app.config['SSO_SERVICE'], app.config['SSO_DOMAIN'],
        app.config.get('SSO_GROUPS'))
    if app.config.get('SSO_DEBUG'):
        app.logger.info(
            'SSO verifier created (service=%s, domain=%s, groups=%s)',
            app.config['SSO_SERVICE'], app.config['SSO_DOMAIN'],
            app.config.get('SSO_GROUPS'))


def sso_api_auth_required(func):
    """Wrap an API (non-interactive) handler with SSO authentication.

    Sets the 'g.current_user' variable to the name of the
    authenticated user, and 'g.sso_ticket' to the SSO ticket itself.
    
    Does not support interactive clients (browsers), so it will not
    support nonces and won't send redirects to the login server.
    Instead, it expects the SSO ticket to be specified as the 'sso'
    attribute of the JSON request payload.

    """
    @wraps(func)
    def _auth_wrapper(*args, **kwargs):
        if current_app.config.get('FAKE_SSO_USER'):
            g.current_user = current_app.config['FAKE_SSO_USER']
            g.sso_ticket = 'sso_ticket'
            return func(*args, **kwargs)

        sso_ticket = request.json.get('sso')
        if not sso_ticket:
            abort(401)
        try:
            ticket = current_app.sso_validator.verify(sso_ticket.encode())
            g.current_user = ticket.user()
            g.sso_is_admin = ('admins' in ticket.groups())
            g.sso_ticket = ticket
        except sso.Error as e:
            current_app.logger.error('authentication failed: %s (ticket=%s)', e, sso_ticket.encode())
            abort(403)
        return func(*args, **kwargs)
    return _auth_wrapper

