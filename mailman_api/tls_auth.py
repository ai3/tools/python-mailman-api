import re
import werkzeug.serving
from functools import wraps
from flask import current_app, request, abort


DEFAULT_TLS_AUTH_ACLS = [('/', '.*')]


class PeerCertWSGIRequestHandler(werkzeug.serving.WSGIRequestHandler):
    """
    We subclass this class so that we can gain access to the connection
    property. self.connection is the underlying client socket. When a TLS
    connection is established, the underlying socket is an instance of
    SSLSocket, which in turn exposes the getpeercert() method.
    The output from that method is what we want to make available elsewhere
    in the application.
    """
    def make_environ(self):
        """
        The superclass method develops the environ hash that eventually
        forms part of the Flask request object.
        We allow the superclass method to run first, then we insert the
        peer certificate into the hash. That exposes it to us later in
        the request variable that Flask provides
        """
        environ = super(PeerCertWSGIRequestHandler, self).make_environ()
        environ['peercert'] = self.connection.getpeercert()
        return environ


def init_tls_auth(app):
    compiled = []
    for acl_path, acl_cn_pattern in app.config.get(
            'TLS_AUTH_ACLS', DEFAULT_TLS_AUTH_ACLS):
        acl_cn_rx = re.compile('^%s$' % acl_cn_pattern)
        compiled.append((acl_path, acl_cn_rx))
    app.tls_auth_acls = compiled


def _get_subject_cn(peercert):
    """Extract subject CN from the parsed peercert data."""
    parsed_subject = peercert['subject']
    if len(parsed_subject) != 1:
        raise Exception('multiple subjects')
    for attr, value in parsed_subject[0]:
        if attr == 'commonName':
            return value


def _regexp_match(rx, s):
    """Returns True if the anchored rx matches s."""
    return rx.match(s) is not None


def tls_auth(fn):
    """Enable TLS client authentication for this endpoint."""
    @wraps(fn)
    def _tls_auth_wrapper(*args, **kwargs):
        cn = _get_subject_cn(request.environ['peercert'])
        for acl_path, acl_cn_rx in current_app.tls_auth_acls:
            if request.path.startswith(acl_path) and _regexp_match(acl_cn_rx, cn):
                return fn(*args, **kwargs)
        current_app.logger.error(
            'unauthenticated mTLS request, path=%s peer=%s', request.path, cn)
        abort(403)
    return _tls_auth_wrapper

