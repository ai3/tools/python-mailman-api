import json
import optparse
import os
import re
import ssl
import subprocess
import sys
import time
import threading
from functools import wraps
from flask import Flask, request, abort, make_response, jsonify, g

from .sso_api import sso_api_auth_required, init_sso
from .tls_auth import tls_auth, init_tls_auth, PeerCertWSGIRequestHandler

sys.path.insert(0, '/var/lib/mailman/bin')
import paths
from Mailman import Errors
from Mailman import MailList
from Mailman import Utils


app = Flask(__name__)
app.config['TLS_AUTH_ACLS'] = [
    ('/', 'client'),
]

# Set up environment for Mailman processes: if we don't explicitly
# control LANG, we're going to get locale setup errors.
mm_env = dict(os.environ)
mm_env['LANG'] = 'C'
mm_env['LC_ALL'] = 'C'

def _run(cmd):
    app.logger.info('running %s', cmd)
    try:
        stderr = subprocess.check_output(
            cmd, env=mm_env, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as e:
        app.logger.error('command FAILED (status=%d), output:\n%s\n',
                         e.returncode, stderr.decode('utf-8'))
        raise


def _random_password():
    return os.urandom(12).encode('base64').rstrip('=\n')


def _maybe_unicode(s):
    # Try optimistically to guess the encoding of the list
    # description (or just don't, nevermind).
    try:
        return unicode(s, 'utf-8')
    except UnicodeDecodeError:
        try:
            return unicode(s, 'iso-8859-1')
        except UnicodeDecodeError:
            pass
    return ''


@app.route('/api/create_list', methods=('POST',))
@tls_auth
def create_list():
    list_addr = request.json['list']
    admin_addr = request.json['admin']
    lang = request.json.get('lang', 'en')

    list_name, list_domain = list_addr.split('@')

    _run([
        '/var/lib/mailman/bin/newlist',
        '--automate',
        '--language=' + lang,
        '--urlhost=' + app.config['MAILMAN_URL_HOST'],
        '--emailhost=' + list_domain,
        list_name,
        admin_addr,
        _random_password(),
    ])

    if os.path.exists('/etc/mailman/forced_params'):
        _run([
            '/var/lib/mailman/bin/config_list',
            '-i', '/etc/mailman/forced_params',
            list_name,
        ])

    app.logger.info('created new list %s', list_addr)
    return jsonify({})


@app.route('/api/get_list_attrs', methods=('POST',))
@tls_auth
def get_list_attrs():
    lists = request.json['lists']
    out = []
    for l in lists:
        m = MailList.MailList(l.split('@')[0], 0)
        attrs = {
            'name': l,
            'public': 'no' if m.archive_private else 'yes',
            'description': _maybe_unicode(m.description),
            'owners': list(set(m.owner[:])),
        }
        out.append(attrs)
    return jsonify(out)


@app.route('/api/pwreset', methods=('POST',))
@tls_auth
@sso_api_auth_required
def pwreset():
    list_addr = request.json['list']
    list_name = list_addr.split('@')[0]
    m = MailList.MailList(list_name, 0)

    if not g.sso_is_admin and not (g.current_user in m.owner):
        app.logger.error('pwreset: %s is not an admin of %s (owners: %s)',
            g.current_user, list_name, m.owner)
        abort(403)

    _run([
        '/var/lib/mailman/bin/change_pw', '-l',
        list_name,
    ])
    return jsonify({})


@app.route('/api/regenerate_archives', methods=('POST',))
@tls_auth
def regenerate_archives():
    list_addr = request.json['list']
    list_name = list_addr.split('@')[0]

    _run([
        '/var/lib/mailman/bin/arch', '--wipe', '--quiet',
        list_name,
    ])
    return jsonify({})


### Instrumentation.

def cache(timeout):
    """Decorator for caching expensive results.

    Allows a single thread to recompute the cached value even in
    presence of concurrent / overlapping requests (will serve stale
    data in the meantime).

    """
    _store = {'deadline': 0, 'value': '', 'lock': threading.Lock()}
    def _cache_wrapper(fn):
        @wraps(fn)
        def _cache(*args, **kwargs):
            now = time.time()
            if now > _store['deadline'] and _store['lock'].acquire(False):
                try:
                    _store['value'] = fn(*args, **kwargs)
                    _store['deadline'] = now + timeout
                finally:
                    _store['lock'].release()
            return _store['value']
        return _cache
    return _cache_wrapper


@app.route('/metrics')
@cache(900)
def metrics():
    out = []
    list_names = Utils.list_names()
    for list_name in list_names:
        try:
            mlist = MailList.MailList(list_name, lock=False)
        except Errors.MMUnknownListError:
            # The list could have been deleted by another process.
            continue

        num_rmembers = len(mlist.getRegularMemberKeys())
        num_dmembers = len(mlist.getDigestMemberKeys())
        out.append('mailman_subscribers{list="%s"} %d\n' % (
            list_name.lower(), num_rmembers + num_dmembers))

    return app.response_class(''.join(out), mimetype='text/plain')


### SSL server with the right parameters.

def serve_ssl(app, host='localhost', port=3000, **kwargs):
    # Create a validating SSLContext.
    ssl_ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    ssl_ctx.set_ciphers('ECDHE-ECDSA-AES256-GCM-SHA384')
    ssl_ctx.load_cert_chain(certfile=app.config['SSL_CERT'],
                            keyfile=app.config['SSL_KEY'])
    ssl_ctx.load_verify_locations(cafile=app.config['SSL_CA'])
    ssl_ctx.verify_mode = ssl.CERT_REQUIRED

    app.run(
        host, port,
        ssl_context=ssl_ctx,
        request_handler=PeerCertWSGIRequestHandler,
        **kwargs
    )


def main():
    parser = optparse.OptionParser()
    parser.add_option('--config', default='/etc/mailman/mailman_api.conf')
    parser.add_option('--port', type='int', default=6088)
    parser.add_option('--addr', default='0.0.0.0')
    opts, args = parser.parse_args()
    if args:
        parser.error('Too many arguments')

    app.config.from_pyfile(opts.config)
    init_sso(app)
    init_tls_auth(app)
    serve_ssl(app, host=opts.addr, port=opts.port)


if __name__ == '__main__':
    main()
